require 'rails_helper'

RSpec.describe "Todos", type: :request do
  let!(:todos) { create_list(:todo, 10) }

  describe 'GET /todos' do
    context '一覧取得に成功した場合' do
      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        get '/todos'

        expect(response.status).to eq 200
        expect(json).to be_present
        expect(json['todos'].size).to eq Todo.count
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context '一覧取得に失敗した場合' do
      example 'status 500を返しerror_code_3が返ること' do
        allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)
        get '/todos'

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 3
        expect(json['error_message']).to eq '一覧の取得に失敗しました'
      end
    end

    context 'その他の不明エラーの場合' do
      example 'status 500を返しerror_code_1が返ること' do
        allow(Todo).to receive(:select).and_raise(Exception)
        get '/todos'

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'POST /todos' do
    let(:valid_attributes) { { title: 'test', detail: 'testdayo', date: '2020-06-01' } }
    let(:invalid_attributes) { { title: '', detail: 'testdayo', date: '2020-06-01' } }

    context 'リクエストが正常な場合' do
      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        post '/todos', params: valid_attributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'リクエストが正常でtitle文字数が100の場合' do
      let(:params) { { title: 't' * 100 } }

      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'リクエストが正常でdetail文字数が1000の場合' do
      let(:params) { { title: 'test', detail: 't' * 1000 } }

      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'リクエストが異常な場合' do
      example 'status 400を返しerror_code_2が返ること' do
        post '/todos', params: invalid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
      end
    end

    context 'リクエストが異常でtitle文字数が100を超えている場合' do
      let(:params) { { title: 't' * 101 } }

      example 'status 400を返しerror_code_2が返ること' do
        post '/todos', params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
      end
    end

    context 'リクエストが異常でdetail文字数が1000を超えている場合' do
      let(:params) { { title: 'test', detail: 't' * 1001 } }

      example 'status 400を返しerror_code_2が返ること' do
        post '/todos', params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
      end
    end

    context '登録に失敗した場合' do
      example 'status 500を返してerror_code_4が返ること' do
        allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)
        post '/todos', params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 4
        expect(json['error_message']).to eq '登録に失敗しました'
      end
    end

    context 'その他の不明エラーの場合' do
      example 'サーバーエラーの場合error_code_1が返ること' do
        allow(Todo).to receive(:create!).and_raise(Exception)
        post '/todos', params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'PUT /todos/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }
    let(:invalid_attributes) { { title: '' } }

    context 'リクエストが正常な場合' do
      let(:todo_id) { todos.first.id }

      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'リクエストが正常でtitle文字数が100ちょうどの場合' do
      let(:todo_id) { todos.first.id }
      let(:params) { { title: 't' * 100 } }

      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'リクエストが正常でdetail文字数が1000ちょうどの場合' do
      let(:todo_id) { todos.first.id }
      let(:params) { { title: 'test', detail: 't' * 1000 } }

      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'リクエストが異常な場合' do
      let(:todo_id) { todos.first.id }

      example 'status 400を返しerror_code_2が返ること' do
        put "/todos/#{todo_id}", params: invalid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
      end
    end

    context 'リクエストが異常でtitle文字数が100を超えている場合' do
      let(:todo_id) { todos.first.id }
      let(:params) { { title: 't' * 101 } }

      example 'status 400を返しerror_code_2が返ること' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
      end
    end

    context 'リクエストが異常でdetail文字数が1000を超えている場合' do
      let(:todo_id) { todos.first.id }
      let(:params) { { title: 'test', detail: 't' * 1001 } }

      example 'status 400を返しerror_code_2が返ること' do
        put "/todos/#{todo_id}", params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
      end
    end

    context '更新に失敗した場合' do
      let(:todo_id) { todos.first.id }

      example 'status 500を返してerror_code_5が返ること' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)
        put "/todos/#{todo_id}"

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 5
        expect(json['error_message']).to eq '更新に失敗しました'
      end
    end

    context 'その他の不明エラーの場合' do
      let(:todo_id) { todos.first.id }

      example 'status 500を返してerror_code_1が返ること' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(Exception)
        put "/todos/#{todo_id}"

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  describe 'DELETE /todos/:id' do
    context '正常な場合' do
      let(:todo_id) { todos.first.id }

      example '成功のレスポンスが返っていればstatus 200を返し、error_code_0が返ること' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context '対象のidがない場合' do
      let(:todo_id) { todos.first.id }
      before { Todo.find(todo_id).destroy }

      example 'status 400を返し、error_code_2が返ってくること' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエスト形式が不正です'
      end
    end

    context '削除に失敗した場合' do
      let(:todo_id) { todos.first.id }

      example 'status 500を返してerror_code_6が返ること' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 6
        expect(json['error_message']).to eq '削除に失敗しました'
      end
    end

    context 'その他の不明エラーの場合' do
      let(:todo_id) { todos.first.id }

      example 'status 500を返してerror_code_1が返ること' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(Exception)
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end
end
